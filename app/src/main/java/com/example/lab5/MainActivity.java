package com.example.lab5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner1;
    private TextView textView1;
    private SeekBar seekBar1;
    private int seekBarVal, period;
    private EditText edit1, edit2, edit3;
    //private int monthly, quarterly, semiannually, annually;
    private int principalAmount, amount, annualInterestRate, p, compoundingFrequency, deposit, frequency, r;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner1 = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.spinner1_items, R.layout.spinner1);

        spinnerAdapter.setDropDownViewResource((R.layout.spinner1));
        spinner1.setAdapter(spinnerAdapter);

        textView1 = (TextView) findViewById(R.id.T5);
        seekBar1 = (SeekBar) findViewById(R.id.seekbar1);

        seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView1.setText("" + progress + " Years");
                seekBarVal = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar1.setMax(10);

        edit1 = (EditText) findViewById(R.id.et1);
        String value1 = edit1.getText().toString();
        int val1 = Integer.parseInt(value1);

        edit2 = (EditText) findViewById(R.id.et2);
        String value2 = edit2.getText().toString();
        int val2 = Integer.parseInt(value2);

        edit3 = (EditText) findViewById(R.id.et3);
        String value3 = edit3.getText().toString();
        int val3 = Integer.parseInt(value3);

        String spinItem = spinner1.getSelectedItem().toString();

        if(spinItem=="Monthly")
             frequency = 12;
        else if(spinItem=="Quarterly")
             frequency = 4;
        else if(spinItem=="Semi Annually")
            frequency = 2;
        else if(spinItem=="Annually")
            frequency = 1;





        principalAmount = val1;
        deposit = val2;
        annualInterestRate = val3;
        period = seekBarVal;
        amount = principalAmount * (1 + r) + (deposit * frequency)* (((1+annualInterestRate)-1)/annualInterestRate);

        r = (annualInterestRate/100)*(1/compoundingFrequency);
        p = period * compoundingFrequency;
        compoundingFrequency = 1;


    }
}
